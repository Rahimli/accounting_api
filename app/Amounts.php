<?php

namespace App;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Amounts extends Model
{
    protected $fillable = [
        'id',
        'amount',
        'currency_id',
        'payment_id',
        'created_at',
    ];



    /**
     * Return the post's author
     */
//    public function currency(): BelongsTo
//    {
//        return $this->belongsTo(Currency::class, 'currency_id');
//    }

    public function payment()
    {
        return $this->belongsTo('App\Payment', 'payment_id');
    }

    public function currency()
    {
        return $this->belongsTo('App\Currency', 'currency_id');
    }

    /**
     * Return the post's author
     */
//    public function payment(): BelongsTo
//    {
//        return $this->belongsTo(Payment::class, 'payment_id');
//    }
}
