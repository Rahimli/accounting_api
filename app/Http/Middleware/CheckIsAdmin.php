<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
//        echo "***************************************************************";
//        echo Auth::user()->type;
//        echo "***************************************************************";
//        dd(Auth::user());
//        dd(Auth::user());
        if (Auth::user()->type == 'admin') {
            return $next($request);
        }

        return redirect()->route('cauth.logout'); // If user is not an admin.
    }
}
