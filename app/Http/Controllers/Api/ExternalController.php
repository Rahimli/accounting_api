<?php


namespace App\Http\Controllers\Api;


use App\Amounts;
use App\Currency;
use App\Http\Controllers\Controller;
use App\Http\Resources\PaymentResourceCollection;
use App\Payment;
use Carbon\Carbon;
use DateTime;
use function Sodium\add;
use Illuminate\Http\Request;

class ExternalController extends Controller
{



    public function index(Request $request)
    {
        // return new PaymentResourceCollection(Payment::paginate());
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $client_id = $request->input('client_id');
        $from = date($from_date);
        $to = date($to_date);
        $result_data = [];
        $income_data =
        $payments = Payment::where(['client_id'=>$client_id])->get();
//        $payments = Payment::whereBetween('created_at', [$from, $to]);

        foreach($payments as $key => $payment_item){
            if(($payment_item ->created_date > $from and $payment_item ->created_date < $to))
                continue;
            $data['client_name'] = $payment_item->client->full_name;
            $data['client_id'] = $payment_item->client_id;
//            $data['worker_name'] = $payment_item->worker->full_name;
            $data['worker_id'] = $payment_item->worker_id;
            $data['received'] = $payment_item->received;
            $data['date'] = $payment_item->created_at;
            $amounts = [];
            $payment_amounts = Amounts::where(['payment_id'=>$payment_item->id])->get();
            foreach ($payment_amounts as $key => $amount_item){
                $amount_data = [];
                $amount_data['id'] = $amount_item->id;
                $amount_data['amount'] = $amount_item->amount;
                $amount_data['currency_title'] = $amount_item->currency->title;
                array_push($amounts,$amount_data);
            }
            $data['amounts'] = $amounts;

            array_push($result_data,$data);
        }
        $result_val['payments'] = $result_data;
        return response()->json(["result"=>'success','data'=>$result_val,'request'=>$request->all()],200);
        return new PaymentResourceCollection(Payment::all());
    }



    public function indexma(Request $request){
//        $payments = Payment::all()->get();
        try{

            $type = $request->input('type');
            $from_date = $request->input('from_date');
            $to_date = $request->input('to_date');
            $from = date($from_date);
            $to_date = date('Y-m-d', strtotime($to_date . ' +1 day'));
            if($type=='client') {
                $client_id = $request->input('client_id');
                $payments = Payment::whereBetween('created_at', [$from, $to_date])->where(['client_id' => $client_id])->get();
            }else if($type == 'worker'){
                $worker_id = $request->input('worker_id');
                $payments = Payment::whereBetween('created_at', [$from, $to_date])->where(['worker_id' => $worker_id])->get();
            }else {
//            $payments = Payment::whereBetween('created_at', [$from, $to_date])->get();
                $payments = Payment::all();
            }

            $returnPayments = [];
            if(count($payments)){
                $status = "success";
                $message = "Payments fetched successfully";
                $status_code = 200;
                foreach ($payments as $paymnt){
                    $amounts = Amounts::where("payment_id",$paymnt->id)->get();
                    $returnAmounts = [];
                    foreach ($amounts as $amount){
//                    $amount->audio_url = site_url().$amount->audio_url;
                        $returnAmounts[] = $amount;
                    }
                    $amount->sub_questions = $returnAmounts;
                    $returnPayments[] = $amount;
                }
            }else{
                $status = "error";
                $message = "No amount found";
                $status_code = 200;
                $payments = null;
            }
        } catch (Exception $e) {
            $status = "error";
            $message = "No amount found";
            $status_code = 200;
            $payments = null;
            $to_date = DateTime()->format('Y-m-d');
//            echo $to_date;
        }
        return response()->json(['status'=>$status,'message'=>$message,'questions'=>$returnPayments],$status_code,["Accept"=>"application/json; charset=utf-8","Content-type"=>"application/json; charset=utf-8"],JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);

    }




    public function dashboardapi(Request $request){
        $data = [];
        $result_data = Array();
        try {
            $client_id = $request->input('client_id');
            $from_date = $request->input('from_date');
            $to_date = $request->input('to_date');
            $from = date($from_date);
            $to_date = date('Y-m-d', strtotime($to_date . ' +1 day'));
//            $to = date($to_date);
            $income_data = [];
            $outcome_data = [];
            $result = [];
            $currecny_array = [];
            $currencies = Currency::all();
            $amounts = Amounts::all();
            foreach ($currencies as $key => $currency_item){
                $currecny_array[$currency_item->id] = ['name'=> $currency_item->title,'key'=> $currency_item->id,];
                $outcome_data[$currency_item->id] = ['name'=> $currency_item->title,'key'=> $currency_item->id,'total'=>0.0];
                $income_data[$currency_item->id] = ['name'=> $currency_item->title,'key'=> $currency_item->id,'total'=>0.0];
            }
//            foreach ($amounts as $key => $amount_item){
//                if ($amount_item->)
//            }
//            $payments = Payment::whereBetween('created_at', [$from, $to])->where()->get();
            $payments = Payment::whereBetween('created_at', [$from, $to_date])->where(['client_id'=>$client_id])->get();
            foreach ($payments as $key => $payment_item){
                    $payment_amounts = Amounts::where(['payment_id'=>$payment_item->id])->get();
                    foreach ($payment_amounts as $key => $amount_item){
                        if($payment_item->received == 1){
                            $income_data[$amount_item->currency_id]['total'] += $amount_item->amount;
                        }else{
                            $outcome_data[$amount_item->currency_id]['total'] += $amount_item->amount;
                        }
//                        $amount_data = [];
//                        $amount_data['id'] = $amount_item->id;
//                        $amount_data['amount'] = $amount_item->amount;
//                        $amount_data['currency_title'] = $amount_item->currency->title;
//                        array_push($amounts,$amount_data);
                    }
//                    $outcome_data[$payment_item->currency_id]['total'] += $payment_item.
            }

            $result_income = [];
            $result_outcome = [];
            foreach ($currencies as $key => $currency_item){
                array_push($result_outcome,$outcome_data[$currency_item->id]);
                array_push($result_income,$income_data[$currency_item->id]);

            }
            $result['income']=$result_income;
            $result['outcome']=$result_outcome;
            return response()->json(["result"=>'success',"data"=>$result,],200);
        } catch (Exception $e) {
            $to_date = DateTime()->format('Y-m-d');
            echo $to_date;
        }


        return response()->json(["result"=>'success','data'=>$result_data],200);
    }


    public function mainapi(Request $request){
        $data = [];
        $result_data = Array();
        try {
            $company = $request->input('company');
            try {
                $from_date = $request->input('from_date');
                if(empty($from_date)){
                    $from_date = Carbon::now()->format('Y-m-d');
                }
            } catch (Exception $e) {
                $from_date = DateTime()->format('Y-m-d');
                echo $from_date;
            }

            $new_data = [];
            $URL = "https://firestore.googleapis.com/v1/projects/".$company."/databases/(default)/documents:runQuery";
            $URL2 = "https://firestore.googleapis.com/v1/projects/".$company."/databases/(default)/documents/payments";
            $data['company'] = $company;
            $data['from_date'] = $from_date."T00:00.00Z";

            $main_param = [
                'structuredQuery'=> [
                    'where' => [
                        'fieldFilter' => [
                            'field' => ['fieldPath' => 'date'],
                            'op' => 'GREATER_THAN',
                            'value' => [
                                'timestampValue' => $from_date."T00:00:00.0Z"
//                                    .format(
//                                    str(isodate.datetime_isoformat(from_date))
//                                # str(isodate.datetime_isoformat(datetime.datetime.now()-datetime.timedelta(days=50))),
//                                )
                            ],
                        ]
                    ],
                    'from' => [['collectionId' => 'payments']]
                ]
            ];


            $payload = json_encode($main_param);

            // Prepare new cURL resource
            $ch = curl_init($URL);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

            // Set HTTP Header for POST request
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($payload))
            );

            // Submit the POST request
            $result = curl_exec($ch);

            // Close cURL session handle
            curl_close($ch);
            $json_data = json_decode($result, true);
            print_r($json_data);
//            die();
            foreach ($json_data as $val_item){
                try{
                    $item = $val_item['document'];

                    $br_date = date(DATE_ISO8601, strtotime($item['fields']['date']['timestampValue'])) ;
//                    if ($br_date >= $start_date) {
                        $new_dict = Array();
                        $new_dict['name'] = explode("/", $item['name'])[sizeof(explode("/", $item['name'])) - 1];
                        $new_dict['createTime'] = $item['createTime'];
                        $new_dict['updateTime'] = $item['updateTime'];
                        $new_dict['workerRef'] = $item['fields']['workerRef']['referenceValue'];
                        $new_dict['workerName'] = $item['fields']['workerName']['stringValue'];
                        $new_dict['clientRef'] = $item['fields']['clientRef']['referenceValue'];
                        $new_dict['clientName'] = $item['fields']['clientName']['stringValue'];
                        $new_dict['confirmed'] = $item['fields']['confirmed']['stringValue'];
                        $new_dict['received'] = $item['fields']['received']['booleanValue'];
                        $new_dict['date'] = $item['fields']['date']['timestampValue'];
                        $new_dict['amounts'] = [];
                        print_r($new_dict);

                        foreach ($item['fields']['amounts']['arrayValue']['values'] as $item_val) {
                            $item_field = $item_val['mapValue']['fields'];
                            try {
                                $new_dict['amounts'] += [
                                    [
                                        'currency' => $item_field['currencyName']['stringValue'],
                                        'amount' => $item_field['amount']['integerValue']
                                    ],
                                ];
                            } catch (\Exception $exception) {

                                $new_dict['amounts'] = [
                                    [
                                        'currency' => $item_field['currencyName']['stringValue'],
                                        'amount' => $item_field['amount']['integerValue']
                                    ],
                                ];
                            }
                        }
                        //                    new_data.append(copy.deepcopy(new_dict))

                        array_push($result_data, $new_dict);
//                    }
//                    $result_data = $result_data.array_push($new_dict);

                }catch (\Exception $exception){

                }
                }

        } catch (Exception $e) {
            $data = ['result'=>'error','message'=>'company has not','data'=>[]];
        }


        return response()->json(["result"=>'success','data'=>$result_data],200);
    }


    public function emergencyapi(Request $request){
        $data = [];
        $result_data = Array();
        try {
            $company = $request->input('company');
            $name_list = $request->input('name_list');
            $name_list = str_replace(" ","",$name_list);
            $name_list = str_replace("]","",$name_list);
            $name_list = str_replace("[","",$name_list);
            $name_list_data =  explode(',',$name_list);
//            print_r($name_list_data);
//            echo $name_list;
            try {
                $from_date = $request->input('date');
                if(empty($from_date)){
                    $from_date = Carbon::now()->format('Y-m-d');
                }
            } catch (Exception $e) {
                $from_date = DateTime()->format('Y-m-d');
                echo $from_date;
            }

            $new_data = [];
            $URL = "https://firestore.googleapis.com/v1/projects/".$company."/databases/(default)/documents:runQuery";
            $data['company'] = $company;
            $data['from_date'] = $from_date."T00:00.00Z";

            $main_param = [
                'structuredQuery' => [
                'where' => [
                    'compositeFilter' =>[
                        'filters' =>[
                                ['fieldFilter' => [
                                    'field' => ['fieldPath' => 'date'],
                                    'op' => 'GREATER_THAN_OR_EQUAL',
                                    'value' => [
                                        'timestampValue' => $from_date.'T00:00:00.0Z'
                                        ],
                                    ]
                                ],
                                ['fieldFilter' => [
                                    'field' => ['fieldPath' => 'date'],
                                    'op' => 'LESS_THAN_OR_EQUAL',
                                    'value' => [
                                        'timestampValue' => $from_date.'T23:59:59.0Z'
                                        ],
                                    ]
                                ]
                            ], 'op' => 'AND'
                        ],

                    ],
                    'from' => [['collectionId' => 'payments']]
                ]
            ];

            $payload = json_encode($main_param);

            // Prepare new cURL resource
            $ch = curl_init($URL);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

            // Set HTTP Header for POST request
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($payload))
            );

            // Submit the POST request
            $result = curl_exec($ch);

            // Close cURL session handle
            curl_close($ch);
            $json_data = json_decode($result, true);
//
            foreach ($json_data as $val_item){
                try{
                $item = $val_item['document'];
                $x_name = explode("/", $item['name'])[sizeof(explode("/", $item['name'])) - 1];
                if (!in_array($x_name, $name_list_data)){
                    $new_dict = Array();
                    $new_dict['name'] = $x_name;
                    $new_dict['createTime'] = $item['createTime'];
                    $new_dict['updateTime'] = $item['updateTime'];
                    $new_dict['workerRef'] = $item['fields']['workerRef']['referenceValue'];
                    $new_dict['workerName'] = $item['fields']['workerName']['stringValue'];
                    $new_dict['clientRef'] = $item['fields']['clientRef']['referenceValue'];
                    $new_dict['clientName'] = $item['fields']['clientName']['stringValue'];
                    $new_dict['confirmed'] = $item['fields']['confirmed']['stringValue'];
                    $new_dict['received'] = $item['fields']['received']['booleanValue'];
                    $new_dict['date'] = $item['fields']['date']['timestampValue'];
                    $new_dict['amounts'] = [];

                    foreach ($item['fields']['amounts']['arrayValue']['values'] as $item_val){
                        $item_field = $item_val['mapValue']['fields'];
                        try {
                            $new_dict['amounts'] += [
                                [
                                    'currency'=>$item_field['currencyName']['stringValue'],
                                    'amount'=>$item_field['amount']['integerValue']
                                ],
                            ];
                        }catch (\Exception $exception) {

                            $new_dict['amounts'] = [
                                [
                                    'currency'=>$item_field['currencyName']['stringValue'],
                                    'amount'=>$item_field['amount']['integerValue']
                                ],
                            ];
                        }
                    }
//                    new_data.append(copy.deepcopy(new_dict))

                    array_push($result_data, $new_dict);
                }
                }catch (\Exception $exception){}
//                    $result_data = $result_data.array_push($new_dict);

            }

        } catch (Exception $e) {
            $data = ['result'=>'error','message'=>'company has not','data'=>[]];
        }


        return response()->json(["result"=>'success','data'=>$result_data],200);
    }
}
