<?php


namespace App\Http\Controllers\Api;


use App\Currency;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Resources\AllUserResourceCollection;

class UserController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }


    public function allusers(Request $request){
        $data = [];
        $result_data = [
        ];
        try {
        $user_type = $request->input('user_type');
        if (!empty($user_type)){
            if($user_type == 'admins' or $user_type == 'all'){
                $result_data['admins'] = User::where('type', '=', 'admin')->get();
            }
            if($user_type == 'workers' or $user_type == 'all'){
                $result_data['workers'] = User::where('type', '=', 'worker')->get();
            }
            if($user_type == 'clients' or $user_type == 'all'){
                $result_data['clients'] = User::where('type', '=', 'client')->get();
            }
        }
        }catch (\Exception $exception){
            return $exception;
        }

        return response()->json(["result"=>'success','data'=>$result_data],200);
    }



    public function allcurrencies(Request $request){
        $data = [];
        $result_data = [
        ];
        $result_data['currencies'] = Currency::all()->get();
        return response()->json(["result"=>'success','data'=>$result_data],200);
    }

}
