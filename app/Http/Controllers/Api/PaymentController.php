<?php

namespace App\Http\Controllers\Api;
use App\Amounts;
use App\Http\Controllers\Controller;

use App\Http\Resources\PaymentResource;
use App\Http\Resources\PaymentResourceCollection;
use App\Payment;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

//    public function index(): PaymentResourceCollection
//    {
//        return new PaymentResourceCollection(Payment::all());
//    }

    public function index(Request $request){
        $user = Auth::user();
        $returnPayments = [];
//        $payments = Payment::all()->get();
        try{

            $type = $request->input('type');
            $from_date = $request->input('from_date');
            $to_date = $request->input('to_date');
            $from = date($from_date);
            $to_date = date('Y-m-d', strtotime($to_date . ' +1 day'));
            if($type=='client') {
                $client_id = $request->input('client_id');
                if($user->type == 'worker'){
                    $payments = Payment::whereBetween('created_at', [$from, $to_date])->where(['client_id' => $client_id])->where(['worker_id' => $user->id,'confirmed'=>'confirmed'])->get();
                }else {
                    $payments = Payment::whereBetween('created_at', [$from, $to_date])->where(['client_id' => $client_id])->get();
                }
            }else if($type == 'worker'){
                $worker_id = $request->input('worker_id');
                $payments = Payment::whereBetween('created_at', [$from, $to_date])->where(['worker_id' => $worker_id])->get();
            }else {
//            $payments = Payment::whereBetween('created_at', [$from, $to_date])->get();
                if($user->type == 'worker'){
                    $payments = Payment::where(['worker_id' => $user->id,'confirmed'=>'confirmed'])->get();
                }else{
                    $payments = Payment::all();
                }
            }

            if(count($payments)){
                $status = "success";
                $message = "Payments fetched successfully";
                $status_code = 200;
                foreach ($payments as $paymnt){
                    $payment_part = [];
                    $amounts = Amounts::where("payment_id",$paymnt->id)->get();
                    $returnAmounts = [];

                    foreach ($amounts as $amount){
//                    $amount->audio_url = site_url().$amount->audio_url;
                        array_push($returnAmounts,['id'=>$amount->id,'amount'=>$amount->amount,'currency_title'=>$amount->currency->title]);
                    }
//                    $amount->sub_questions = $returnAmounts;
                    $payment_part['amounts'] = $returnAmounts;
                    $payment_part['id'] = $paymnt->id;
                    $payment_part['client_id'] = $paymnt->client_id;
                    $payment_part['worker_id'] = $paymnt->worker_id;
                    $payment_part['received'] = $paymnt->received;
                    $payment_part['client_name'] = $paymnt->client->full_name;
                    $payment_part['confirmed'] = $paymnt->confirmed;
                    $payment_part['date'] = Carbon::parse($paymnt->created_at)->format('d M, Y H:i');
                    array_push($returnPayments,$payment_part);
//                    $returnPayments['amounts'] = $returnAmounts;
                }
            }else{
                $status = "error";
                $message = "No amount found";
                $status_code = 200;
                $payments = null;
            }
        } catch (Exception $e) {
            $status = "error";
            $message = "No amount found";
            $status_code = 200;
            $payments = null;
            $to_date = DateTime()->format('Y-m-d');
//            echo $to_date;
        }
        $returnData['payments'] = $returnPayments;
        return response()->json(['status'=>$status,'message'=>$message,'data'=>$returnData],$status_code,["Accept"=>"application/json; charset=utf-8","Content-type"=>"application/json; charset=utf-8"],JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);

    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function create()
//    {
//        //
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)//: PaymentResource
    {
        $request->validate([
            'client_id'     =>      'required',
            'worker_id'     =>      'required',
            'received'      =>      'required',
            'confirmed'     =>      'required',
            'currency_id'   =>      'required',
        ]);

        $payment = Payment::create($request->all());
//
        $amounts = $request->input('amounts');

        foreach ($amounts as $key => $amount_item){
            $amount_item['payment_id'] = $payment->id;
            Amounts::create($amount_item);
        }

//        return new PaymentResource($payment);
        return response()->json(['message' => $request->all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment): PaymentResource
    {
        return new PaymentResource($payment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function edit($id)
//    {
//        //
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request):bool
    {
        $request->validate([
            'id'     =>      'required',
            'client_id'     =>      'required',
            'confirmed'      =>      'required',
        ]);
        // dd($request->all());
        $id = $request->input('id');
        $client_id = $request->input('client_id');
        $payment = Payment::find($id);
        if($payment->client_id == $client_id){
            $payment->update($request->all());
        }
        return true;
    }


//     public function update(Payment $payment, Request $request):PaymentResource
//     {
// //        dd($request->all());

//         $request->validate([
//             'id'     =>      'required',
//             'client_id'     =>      'required',
//             'confirmed'      =>      'required',
//         ]);

//         $id = $request->input('id');
//         $client_id = $request->input('client_id');
//     	$payment = Payment::find($id)
//     	if($payment->client_id == $client_id)
//         $payment->update($request->all());
//         return new PaymentResource($payment);
//     }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        $payment->delete();
        return response()->json();
    }
}
