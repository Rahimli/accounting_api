<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CurrenciesRequest;
use App\Currency;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class CurrencyController extends Controller
{
    /**
     * Show the application currencies index.
     */
    public function index(): View
    {
        return view('admin.currencies.index', [
//            'currencies' => Currency::where()->paginate(15),
            'currencies' => DB::table('currencies')->paginate(15),
//            'currencies' => Currency::withCount('comments', 'likes')->with('author')->latest()->paginate(50)
        ]);
    }

    /**
     * Display the specified resource edit form.
     */
    public function edit(Currency $currency): View
    {
        return view('admin.currencies.edit', [
            'currency' => $currency,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request): View
    {
        return view('admin.currencies.create', [
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CurrenciesRequest $request): RedirectResponse
    {
        $currency = Currency::create($request->only(['title', 'content', 'posted_at', 'author_id', 'thumbnail_id']));

        return redirect()->route('admin.currencies.edit', $currency)->withSuccess(__('currencies.created'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CurrenciesRequest $request, Currency $currency): RedirectResponse
    {
        $currency->update($request->only(['title', 'content', 'posted_at', 'author_id', 'thumbnail_id']));

        return redirect()->route('admin.currencies.edit', $currency)->withSuccess(__('currencies.updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Currency  $currency)
    {
        $currency->delete();

        return redirect()->route('admin.currencies.index')->withSuccess(__('currencies.deleted'));
    }
}
