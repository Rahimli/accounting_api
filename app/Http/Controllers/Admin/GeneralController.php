<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class GeneralController extends Controller
{


    public function index(): View
    {

//        dd(Auth::user());
        return view('admin.general.index', [
            'admin_count' => User::where('type', '=', 'admin')->count(),
            'worker_count' => User::where('type', '=', 'worker')->count(),
            'client_count' => User::where('type', '=', 'client')->count(),
//            'comments' =>  Comment::lastWeek()->get(),
//            'posts' => Post::lastWeek()->get(),
//            'users' => User::lastWeek()->get(),
        ]);
    }
    public function admins(): View
    {
        return view('admin.general.admins', [
            'users' => User::where('type', '=', 'admin')->paginate(15),
//            'currencies' => Currency::where()->paginate(15),
//            'users' => User::where('type', '=', 'admin')->paginate(15),
//            'currencies' => Currency::withCount('comments', 'likes')->with('author')->latest()->paginate(50)
        ]);
    }
    public function workers(): View
    {
        return view('admin.general.workers', [
//            'currencies' => Currency::where()->paginate(15),
            'users' => User::where('type', '=', 'worker')->paginate(15),
//            'currencies' => Currency::withCount('comments', 'likes')->with('author')->latest()->paginate(50)
        ]);
    }
    public function clients(): View
    {
        return view('admin.general.clients', [
//            'currencies' => Currency::where()->paginate(15),
            'users' => User::where('type', '=', 'client')->paginate(15),
//            'currencies' => Currency::withCount('comments', 'likes')->with('author')->latest()->paginate(50)
        ]);
    }

    public function changeuser(Request $request,User $user)
    {
        $all_requests = $request->all();
        $type = $all_requests['type'];
        $value = $all_requests['value'];
        if($type == 'can-create-worker'){
                $user->can_create_users = $value;
                $user->save();
        } else if($type == 'can-create-client'){
                $user->can_create_clients = $value;
                $user->save();
        } else if($type == 'can-confirm-payments'){
                $user->can_confirm_payments = $value;
                $user->save();
        }
//        echo $all_requests;
        return response()->json($all_requests,200,["Accept"=>"application/json; charset=utf-8","Content-type"=>"application/json; charset=utf-8"],JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
//        return response()->json([
//            'name' => 'Abigail',
//            'state' => 'CA'
//        ]);
    }
}
