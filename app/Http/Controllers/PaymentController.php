<?php

namespace App\Http\Controllers\Api;
use App\Amounts;
use App\Http\Controllers\Controller;

use App\Http\Resources\PaymentResource;
use App\Http\Resources\PaymentResourceCollection;
use App\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function __construct()
    // {
    //     $this->middleware('auth:api', ['except' => ['login']]);
    // }

    public function index(Request $request)
    {
        // return new PaymentResourceCollection(Payment::paginate());
        $from_date = $request->input('from_date');
        $to_date = $request->input('to_date');
        $from = date($from_date);
        $to = date($to_date);
        $result_data = [];
        $payments = Payment::all();
//        $payments = Payment::whereBetween('created_at', [$from, $to]);

        foreach($payments as $key => $payment_item){
            if(($payment_item ->created_date > $from and $payment_item ->created_date < $to))
                continue;
            $data['client_name'] = $payment_item->client->full_name;
            $data['client_id'] = $payment_item->client_id;
//            $data['worker_name'] = $payment_item->worker->full_name;
            $data['worker_id'] = $payment_item->worker_id;
            $data['received'] = $payment_item->received;
            $data['date'] = $payment_item->created_at;
            $amounts = [];
            $payment_amounts = Amounts::where(['payment_id'=>$payment_item->id])->get();
            foreach ($payment_amounts as $key => $amount_item){
                $amount_data = [];
                $amount_data['id'] = $amount_item->id;
                $amount_data['amount'] = $amount_item->amount;
                $amount_data['currency_title'] = $amount_item->currency->title;
                array_push($amounts,$amount_data);
            }
            $data['amounts'] = $amounts;

            array_push($result_data,$data);
        }
        $result_val['payments'] = $result_data;
        return response()->json(["result"=>'success','data'=>$result_val,'request'=>$request->all()],200);
        return new PaymentResourceCollection(Payment::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function create()
//    {
//        //
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'client_id'     =>      'required',
            'worker_id'     =>      'required',
            'received'      =>      'required',
            'confirmed'     =>      'required',
//            'currency_id'   =>      'required',
        ]);

        $payment = Payment::create($request->all());

        $amounts = $request->all()['amounts'];

        foreach ($amounts as $index => $amount_item) {
            $amount = Amounts::create(array_merge($amounts[$index],['payment_id'=>$payment->id]));
        }

        return response()->json(["result"=>'success','data'=>$amounts],200);
        return new PaymentResource($payment);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment): PaymentResource
    {
        return new PaymentResource($payment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function edit($id)
//    {
//        //
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Payment $payment, Request $request):PaymentResource
    {
//        dd($request->all());
        $payment->update($request->all());
        return new PaymentResource($payment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        $payment->delete();
        return response()->json();
    }
}
