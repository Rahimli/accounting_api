<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'id',
        'client_id',
        'worker_id',
        'received',
        'confirmed',
        'currency_id',
        'created_at',
    ];



    /**
     * Return the post's author
     */
    public function client()
    {
        return $this->belongsTo('App\User', 'client_id');
//        return $this->belongsTo(User::class, 'client_id');
    }
    public function amounts()
    {
        return $this->hasMany('App\Amounts');
    }
    public function worker()
    {
        return $this->belongsTo('App\User', 'worker_id');
    }
}
