@extends('admin.layouts.app')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Currencies
                <small>List</small>
            </h1>
{{--            <ol class="breadcrumb">--}}
{{--                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
{{--                <li><a href="#">Tables</a></li>--}}
{{--                <li class="active">Simple</li>--}}
{{--            </ol>--}}
        </section>
    @include ('admin/currencies/_list')
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
