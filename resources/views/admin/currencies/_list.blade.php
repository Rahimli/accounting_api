
<!-- Main content -->
<section class="content">
    <div class="row">
            <div class="col-md-8"></div>
            <div class="col-md-4">
                <a style="margin: 10px 0;" href="{{ route('currencies.create') }}" class="btn btn-block btn-success">Create</a>

            </div>
        <hr/>
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Currencies</h3>
                    <div class="box-tools">
                        {{ $currencies->links() }}
{{--                        <ul class="pagination pagination-sm no-margin pull-right">--}}
{{--                            <li><a href="#">&laquo;</a></li>--}}
{{--                            <li><a href="#">1</a></li>--}}
{{--                            <li><a href="#">2</a></li>--}}
{{--                            <li><a href="#">3</a></li>--}}
{{--                            <li><a href="#">&raquo;</a></li>--}}
{{--                        </ul>--}}
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered"><tr>
                            <th>Title</th>
                            <th>Date</th>
                            <th>Progress</th>
                        </tr>
                        @foreach($currencies as $currency)
                        <tr>
                            <td>{{ $currency->title }}</td>
                            <td>{{ $currency->created_date }}</td>
                            <td>
{{--                                {{ link_to_route('admin.currencies.edit', $post->id->fullname) }}--}}
                            </td>
                        </tr>

                        @endforeach
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    {{ $currencies->links() }}
{{--                    <ul class="pagination pagination-sm no-margin pull-right">--}}
{{--                        <li><a href="#">&laquo;</a></li>--}}
{{--                        <li><a href="#">1</a></li>--}}
{{--                        <li><a href="#">2</a></li>--}}
{{--                        <li><a href="#">3</a></li>--}}
{{--                        <li><a href="#">&raquo;</a></li>--}}
{{--                    </ul>--}}
                </div>
            </div>
            <!-- /.box -->

        </div>
        <!-- /.col -->
    </div>
</section>
