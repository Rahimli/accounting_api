@extends('admin.layouts.app')
@section('title')
    Clients
@endsection
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Clients
                <small>List</small>
            </h1>
            {{--            <ol class="breadcrumb">--}}
            {{--                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
            {{--                <li><a href="#">Tables</a></li>--}}
            {{--                <li class="active">Simple</li>--}}
            {{--            </ol>--}}
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Clients</h3>
                            <div class="box-tools">
                                {{ $users->links() }}
                                {{--                        <ul class="pagination pagination-sm no-margin pull-right">--}}
                                {{--                            <li><a href="#">&laquo;</a></li>--}}
                                {{--                            <li><a href="#">1</a></li>--}}
                                {{--                            <li><a href="#">2</a></li>--}}
                                {{--                            <li><a href="#">3</a></li>--}}
                                {{--                            <li><a href="#">&raquo;</a></li>--}}
                                {{--                        </ul>--}}
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                </tr>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                    </tr>

                                @endforeach
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $users->links() }}
                            {{--                    <ul class="pagination pagination-sm no-margin pull-right">--}}
                            {{--                        <li><a href="#">&laquo;</a></li>--}}
                            {{--                        <li><a href="#">1</a></li>--}}
                            {{--                        <li><a href="#">2</a></li>--}}
                            {{--                        <li><a href="#">3</a></li>--}}
                            {{--                        <li><a href="#">&raquo;</a></li>--}}
                            {{--                    </ul>--}}
                        </div>
                    </div>
                    <!-- /.box -->

                </div>
                <!-- /.col -->
            </div>
        </section>

        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
