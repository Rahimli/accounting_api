@extends('admin.layouts.app')

@section('title')
    Admins
@endsection
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Admins
                <small>List</small>
            </h1>
            {{--            <ol class="breadcrumb">--}}
            {{--                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>--}}
            {{--                <li><a href="#">Tables</a></li>--}}
            {{--                <li class="active">Simple</li>--}}
            {{--            </ol>--}}
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Admins</h3>
                            <div class="box-tools">
                                {{ $users->links() }}
                                {{--                        <ul class="pagination pagination-sm no-margin pull-right">--}}
                                {{--                            <li><a href="#">&laquo;</a></li>--}}
                                {{--                            <li><a href="#">1</a></li>--}}
                                {{--                            <li><a href="#">2</a></li>--}}
                                {{--                            <li><a href="#">3</a></li>--}}
                                {{--                            <li><a href="#">&raquo;</a></li>--}}
                                {{--                        </ul>--}}
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered"><tr>
                                    <th>Name @csrf</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Can Create Worker</th>
                                    <th>Can Create Client</th>
                                </tr>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->phone }}</td>
                                        <td>
                                            <div class="form-group">

                                                <label>
                                                    <input type="checkbox" id="user-list-item-1-{{ $user->id }}" onchange="changeVal('user-list-item-1-{{ $user->id }}','{{ route('general.changeuser',['user' => $user])  }}','can-create-worker')"  class="" @if($user->can_create_users == 1) checked @endif>
                                                    Can Create Worker
                                                </label>
                                            </div>
                                            {{--                                {{ link_to_route('admin.currencies.edit', $post->id->fullname) }}--}}
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <label>
                                                    <input type="checkbox" id="user-list-item-2-{{ $user->id }}" onchange="changeVal('user-list-item-2-{{ $user->id }}','{{ route('general.changeuser',['user' => $user])  }}','can-create-client')"  class="" @if($user->can_create_clients == 1) checked @endif>
                                                    Can Create Client
                                                </label>
                                            </div>
                                            {{--                                {{ link_to_route('admin.currencies.edit', $post->id->fullname) }}--}}
                                        </td>
                                    </tr>

                                @endforeach
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            {{ $users->links() }}
                            {{--                    <ul class="pagination pagination-sm no-margin pull-right">--}}
                            {{--                        <li><a href="#">&laquo;</a></li>--}}
                            {{--                        <li><a href="#">1</a></li>--}}
                            {{--                        <li><a href="#">2</a></li>--}}
                            {{--                        <li><a href="#">3</a></li>--}}
                            {{--                        <li><a href="#">&raquo;</a></li>--}}
                            {{--                    </ul>--}}
                        </div>
                    </div>
                    <!-- /.box -->

                </div>
                <!-- /.col -->
            </div>
        </section>

        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection


@section('js')
    <script>
        function changeVal($id,$url,$type) {
            var value_el = 0;
            if(document.getElementById($id).checked){
                value_el = 1;
            }else{
                value_el = 0;
            }
            $.post($url,{'_token':'{{ csrf_token() }}','type':$type,'value':value_el})
        }
    </script>
@endsection
