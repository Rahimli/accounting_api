@extends('admin.layouts.app')
@section('title')
    Dashboard
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
            </h1>
            <ol class="breadcrumb">
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Info boxes -->
            <div class="row">
                <!-- /.col -->
                <a href="{{ route('general.admins')  }}" class="col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="fa fa-user"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Admins</span>
                            <span class="info-box-number">{{ $admin_count  }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </a>
                <!-- /.col -->

                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

                <a href="{{ route('general.workers')  }}" class="col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green">
                            <i class="fa fa-users"></i>
                        </span>

                        <div class="info-box-content">
                            <span class="info-box-text">Workers</span>
                            <span class="info-box-number">{{ $worker_count  }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </a>
                <!-- /.col -->
                <a href="{{ route('general.clients')  }}" class="col-md-4 col-sm-4 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="fa fa-address-card"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Clients</span>
                            <span class="info-box-number">{{ $client_count  }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </a>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
