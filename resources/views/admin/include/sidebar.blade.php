<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="{{ route('general.dashboard')  }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    <span class="pull-right-container">
                    </span>
                </a>
            </li>
            <li>
                <a href="{{ route('general.admins')  }}">
                    <i class="fa fa-user"></i> <span>Admins</span>
                    <span class="pull-right-container">
                    </span>
                </a>
            </li>
            <li>
                <a href="{{ route('general.workers')  }}">
                    <i class="fa fa-users"></i> <span>Workers</span>
                    <span class="pull-right-container">
                    </span>
                </a>
            </li>
            <li>
                <a href="{{ route('general.clients')  }}">
                    <i class="fa fa-address-card"></i> <span>Clients</span>
                    <span class="pull-right-container">
                    </span>
                </a>
            </li>
{{--            <li class="treeview">--}}
{{--                <a href="#">--}}
{{--                    <i class="fa fa-users"></i>--}}
{{--                    <span>Users</span>--}}
{{--                    <span class="pull-right-container">--}}
{{--              <i class="fa fa-angle-left pull-right"></i>--}}
{{--            </span>--}}
{{--                </a>--}}
{{--                <ul class="treeview-menu">--}}
{{--                    <li><a href=""><i class="fa fa-circle-o"></i> list</a></li>--}}
{{--                    <li><a href=""><i class="fa fa-circle-o"></i> create</a></li>--}}
{{--                </ul>--}}
{{--            </li>--}}
{{--            <li class="treeview">--}}
{{--                <a href="#">--}}
{{--                    <i class="fa fa-users"></i>--}}
{{--                    <span>Payments</span>--}}
{{--                    <span class="pull-right-container">--}}
{{--              <i class="fa fa-angle-left pull-right"></i>--}}
{{--            </span>--}}
{{--                </a>--}}
{{--                <ul class="treeview-menu">--}}
{{--                    <li><a href=""><i class="fa fa-circle-o"></i> list</a></li>--}}
{{--                    <li><a href=""><i class="fa fa-circle-o"></i> create</a></li>--}}
{{--                </ul>--}}
{{--            </li>--}}
{{--            <li class="treeview">--}}
{{--                <a href="#">--}}
{{--                    <i class="fa fa-users"></i>--}}
{{--                    <span>Currencies</span>--}}
{{--                    <span class="pull-right-container">--}}
{{--              <i class="fa fa-angle-left pull-right"></i>--}}
{{--            </span>--}}
{{--                </a>--}}
{{--                <ul class="treeview-menu">--}}
{{--                    <li><a href=""><i class="fa fa-circle-o"></i> list</a></li>--}}
{{--                    <li><a href=""><i class="fa fa-circle-o"></i> create</a></li>--}}
{{--                </ul>--}}
{{--            </li>--}}
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
