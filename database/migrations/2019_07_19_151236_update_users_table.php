<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('can_confirm_payments')->default(false);
            $table->boolean('can_create_clients')->default(false);
            $table->boolean('can_create_users')->default(false);
            $table->unsignedInteger('created_by_id')->unsigned()->nullable();
            $table->foreign('created_by_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->string('phone', 100)->nullable();


            $table->string('api_token', 60)->unique()->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('can_confirm_payments');
            $table->dropColumn('can_create_clients');
            $table->dropColumn('can_create_users');
            $table->dropColumn('created_by_id');
            $table->dropColumn('phone');
            $table->dropColumn('api_token');
        });
    }
}
