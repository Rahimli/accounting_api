<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//
//});


//Route::post('me', 'Api\AuthController@me')->name('api.profile');
Route::get('main-api', 'Api\ExternalController@mainapi')->name('api.mainapi');
Route::get('emergency-api', 'Api\ExternalController@emergencyapi')->name('api.emergencyapi');
Route::get('dashboardapi', 'Api\ExternalController@dashboardapi')->name('api.dashboardapi');
Route::post('allusers', 'Api\UserController@allusers')->name('api.allusers');
Route::post('allcurrencies', 'Api\UserController@allcurrencies')->name('api.allcurrencies');


//Route::post('login', 'APILoginController@login');


//Route::group([
//
//    'middleware' => 'api',
//    'prefix' => 'auth'
//
//], function ($router) {
//
//    Route::post('login', 'Api\AuthController@login');
//    Route::post('logout', 'Api\AuthController@logout');
//    Route::post('refresh', 'Api\AuthController@refresh');
//    Route::post('me', 'Api\AuthController@me');
//
//});

Route::apiResource('/payments','Api\PaymentController');
Route::post('payment-update', 'Api\PaymentController@update')->name('payment.update');

//Route::prefix('v1')->group(function () {
//    Route::prefix('auth')->group(function () {

Route::group(['guard' => 'api'], function () {

    Route::group(['prefix' => 'auth'], function ($router) {
        Route::post('login', 'Api\AuthController@login')->name('api.login');
        Route::post('logout', 'Api\AuthController@logout')->name('api.logout');
        Route::post('refresh', 'Api\AuthController@refresh')->name('api.refresh');
        Route::post('me', 'Api\AuthController@me')->name('api.profile');
//        Route::post('register','Api\AuthController@register')->name('api.register');
    });


});
