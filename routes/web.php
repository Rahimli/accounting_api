<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
//    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//    Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
//    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
//    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
//    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
//    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::group(['middleware' => ['auth',]], function () {
    Route::get('/log-out', 'Auth\LoginController@logout')->name('cauth.logout');
});

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => ['auth','is.admin']], function () {
//    Route::resource('currencies', 'CurrencyController');
    Route::get('/', 'Admin\GeneralController@index')->name('general.dashboard');
    Route::get('admin', 'Admin\GeneralController@index')->name('general.dashboard');
    Route::get('admin/admins', 'Admin\GeneralController@admins')->name('general.admins');
    Route::get('admin/workers', 'Admin\GeneralController@workers')->name('general.workers');
    Route::get('admin/clients', 'Admin\GeneralController@clients')->name('general.clients');
    Route::post('admin/change-val/{user}', 'Admin\GeneralController@changeuser')->name('general.changeuser');
//    Route::get('/home', 'HomeController@index')->name('home');
//    Route::get('admin/dashboard', 'Admin.ShowDashboard')->name('dashboard');
//    Route::get('/user/{data}', 'UserController@getData');
//    Route::post('/user/{data}', 'UserController@postData');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
